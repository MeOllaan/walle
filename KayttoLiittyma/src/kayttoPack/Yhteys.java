package kayttoPack;


import java.io.*;

import lejos.pc.comm.NXTConnector;


public class Yhteys {

    public NXTConnector conn = new NXTConnector();
    public DataInputStream dis;
    public DataOutputStream dos;
    public boolean connected;

   
    // Luodaan USB -yhteys
    public void Connect() {

        boolean connected = conn.connectTo("usb://");

        if (!connected) {
            System.err.println("Yhdistäminen epäonnistui");
        } else {
            System.out.println("USB yhteys luotu");
        }

        dis = new DataInputStream(conn.getInputStream());
        dos = new DataOutputStream(conn.getOutputStream());

    }

}
