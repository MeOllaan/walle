package Robotti;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;


public class Kirjoita {
	static final String fileName = "config.ini";
	static String text;
	static byte[] byteText;
	static FileOutputStream fos;
	static File f;
	static StringBuffer sb;
	static int fileVersion;
	public String tiedot;
	
	// Muutetaan saadut muuttujat biteiksi
    static private byte[] getBytes(String inputText){
    	//Debug Point
        byte[] nameBytes = new byte[inputText.length()+1];
        
        for(int i=0;i<inputText.length();i++){
            nameBytes[i] = (byte) inputText.charAt(i);
        }
        nameBytes[inputText.length()] = 0;
 
        return nameBytes;
    }
    
	// Lis�t��n getBytes -metodista saadut bittimuotoiset tiedot tiedostoon
    
    private static void appendToFile(String text) throws IOException{
        byteText = getBytes(text);

       
        for(int i=0;i<byteText.length-1;i++){
            fos.write((int) byteText[i]);
        }    	
    }
    

    
     
    private String getKML(String tiedot){
    
    	sb = new StringBuffer();
        
        sb.append(tiedot);
        
        
        return sb.toString();
    }
    
    
    // Kirjoita metodi joka kirjoittaa saadut tiedot (data parametri) config tiedostoksi
   
    public void kirjoitaFile(String data)throws Exception {
    	String tiedot = data;
    	fileVersion = 1;
          
        LCD.drawString("Luodaan tiedostoa",0,1);
        LCD.refresh();
        
        try{
            f = new File(fileName);
            fos = new  FileOutputStream(f);
            // saadut arvot l�hetet��n metodiin jossa ne alustetaan kirjoitettavaan muotoon
            text = getKML(tiedot);
            
            // muutetaan tiedot biteiksi ja kirjoitetaan tiedostoon
            appendToFile(text);
            
            fos.close();

        }catch(IOException e){
            LCD.drawString(e.getMessage(),0,0);
        }
        
        LCD.drawString("Tiedosto luotu",0,4);
     
        Thread.sleep(2000);
        LCD.clear();
    }
}
